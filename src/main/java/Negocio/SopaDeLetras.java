package Negocio;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (Juan SebastiÃ¡n SÃ¡nchez Prada-1151771) (Brayan Camilo
 * Galvan-1151776)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];
    private int sopaPintada[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }
        this.sopaPintada = new int[this.sopas.length][this.sopas[0].length];
    }

    public SopaDeLetras(char[][] excel) {
        this.sopas = excel;
        sopaPintada = new int[this.sopas.length][this.sopas[0].length];
    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada() {
        for (int i = 0; i < this.sopas.length; i++) {
            if (this.sopas.length != this.sopas[i].length) {
                return false;
            }
        }
        return true;
    }

    public boolean esDispersa() {
        return !(this.esCuadrada() || this.esRectangular());
    }

    public boolean esRectangular() {
        int x = this.sopas[0].length;
        for (int i = 0; i < this.sopas.length; i++) {
            if (this.sopas.length == x || x != this.sopas[i].length) {
                return false;
            }
            x = this.sopas[i].length;
        }
        return true;
    }

    public String horizontales(String palabra) throws Exception {
        if (palabra == null || palabra.isEmpty()) {
            throw new Exception("Error, ingrese una palabra o letra");
        }

        String msg = "";
        char letras[] = palabra.toCharArray();

        for (int i = 0; i < this.sopas.length; i++) {
            msg += vectorHorizontal(letras, sopas[i], i);
            msg += vectorHorizontalInverso(letras, sopas[i], i);
        }
        return msg;
    }

    private String vectorHorizontal(char letras[], char vector[], int fila) {
        String mensaje = "";

        for (int i = 0; i < vector.length; i++) {
            if (estaHorizontal(i, vector, letras, fila) == true && (vector.length - i) >= letras.length) {
                mensaje += "- Horizontal Izq a Der: Posición x:" + fila + ", posicion y:" + i + "\n";
            }
        }
        return mensaje;
    }

    private boolean estaHorizontal(int posicionVector, char vector[], char letras[], int fila) {
        int posicionLetras = 0;
        int posicionInicial = posicionVector;
        for (int i = 0; i < vector.length; i++) {

            while (posicionVector < vector.length && posicionLetras < letras.length) {
                if (vector[posicionVector] != letras[posicionLetras]) {
                    return false;
                }
                posicionLetras++;
                posicionVector++;
            }
        }
        if ((vector.length - posicionInicial) >= letras.length) {
            pintarHorizontal(fila, posicionInicial, letras.length);
        }
        return true;
    }

    private void pintarHorizontal(int fila, int columna, int tamanioPalabra) {
        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            columna++;
        }
    }

    private String vectorHorizontalInverso(char letras[], char vector[], int fila) {
        String mensaje = "";

        for (int i = vector.length - 1; i >= 0; i--) {
            if (estaHorizontalInversa(i, vector, letras, fila) == true && (i + 1 >= letras.length)) {
                mensaje += "- Horizontal Der a Izq: Posición x:" + fila + ", posicion y:" + i + "\n";
            }
        }
        return mensaje;
    }

    private boolean estaHorizontalInversa(int posicionVector, char vector[], char letras[], int fila) {
        int posicionLetras = 0;
        int posicionInicial = posicionVector;
        while (posicionLetras < letras.length && posicionVector >= 0) {
            if (vector[posicionVector] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionVector--;
        }

        if ((posicionInicial + 1 >= letras.length)) {
            pintarHorizontalInversa(fila, posicionInicial, letras.length);

        }
        return true;
    }

    private void pintarHorizontalInversa(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            columna--;
        }
    }

    public String verticales(String palabra) throws Exception {
        if (palabra == null || palabra.isEmpty()) {
            throw new Exception("Error, ingrese una palabra o letra");
        }

        String msg = "";
        char letras[] = palabra.toCharArray();

        for (int i = 0; i < this.sopas[0].length; i++) {
            msg += vectorVertical(i, letras);
            msg += vectorVerticalInversa(i, letras);
        }
        return msg;
    }

    private String vectorVertical(int posicionColumna, char letras[]) {
        String msg = "";

        for (int i = 0; i < sopas.length; i++) {
            if (estaVertical(i, posicionColumna, letras) == true && (sopas.length - i) >= letras.length) {
                msg += "- Vertical Arriba a Abajo: Posición x:" + i + ", posicion y:" + posicionColumna + "\n";
            }
        }
        return msg;
    }

    private boolean estaVertical(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicial = posicionFila;
        while (posicionFila < sopas.length && posicionLetras < letras.length) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila++;
        }

        if ((sopas.length - posicionInicial) >= letras.length) {
            pintarVertical(posicionInicial, posicionColumna, letras.length);

        }
        return true;
    }

    private void pintarVertical(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila++;
        }
    }

    private String vectorVerticalInversa(int posicionColumna, char letras[]) {
        String msg = "";

        for (int i = sopas.length - 1; i >= 0; i--) {
            if (estaVerticalInversa(i, posicionColumna, letras) == true && (i + 1) >= letras.length) {
                msg += "- Vertical Abajo a Arriba: Posición x:" + i + ", posicion y:" + posicionColumna + "\n";
            }
        }
        return msg;
    }

    private boolean estaVerticalInversa(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicial = posicionFila;
        while (posicionLetras < letras.length && posicionFila >= 0) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila--;
        }

        if ((posicionInicial + 1) >= letras.length) {
            pintarVerticalInversa(posicionInicial, posicionColumna, letras.length);

        }
        return true;
    }

    private void pintarVerticalInversa(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila--;
        }
    }

    public String diagonalesIzquierdaDerecha(String palabra) throws Exception {
        if (palabra == null || palabra.isEmpty()) {
            throw new Exception("Error, ingrese una palabra o letra");
        }

        String msg = "";
        char letras[] = palabra.toCharArray();

        for (int i = 0; i < this.sopas[0].length; i++) {
            msg += vectorDiagonalIzquierdaDerecha(i, letras);
            msg += vectorDiagonalIzquierdaDerechaInversa(i, letras);
        }
        return msg;
    }

    private String vectorDiagonalIzquierdaDerecha(int posicionColumna, char letras[]) {
        String msg = "";

        for (int i = 0; i < sopas.length; i++) {
            boolean validacion = (sopas.length - i) >= letras.length && (sopas[0].length - posicionColumna) >= letras.length;
            if (estaDiagonalIzquierdaDerecha(i, posicionColumna, letras) && validacion) {
                msg += "- Diagonal Izquierda a Derecha y de Arriba hacía Abajo: Desde posición x:" + i + ", posicion y:" + posicionColumna + 
                        ", Hasta posición x1:"+(i+(letras.length-1))+", posición y1:"+(posicionColumna+(letras.length-1))+"\n";
            }
        }
        return msg;
    }

    private boolean estaDiagonalIzquierdaDerecha(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicialFila = posicionFila;
        int posicionInicialColumna = posicionColumna;
        while (posicionLetras < letras.length && posicionFila < sopas.length && posicionColumna < sopas[0].length) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila++;
            posicionColumna++;
        }

        if ((sopas.length - posicionInicialFila) >= letras.length && (sopas[0].length - posicionInicialColumna) >= letras.length) {
            pintarDiagonalIzquierdaDerecha(posicionInicialFila, posicionInicialColumna, letras.length);

        }
        return true;
    }

    private void pintarDiagonalIzquierdaDerecha(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila++;
            columna++;
        }
    }

    private String vectorDiagonalIzquierdaDerechaInversa(int posicionColumna, char letras[]) {
        String msg = "";

        for (int i = sopas.length - 1; i >= 0; i--) {
            boolean validacion = (i + 1) >= letras.length && (sopas[0].length - posicionColumna) >= letras.length;
            if (estaDiagonalIzquierdaDerechaInversa(i, posicionColumna, letras) && validacion) {
                msg += "- Diagonal Izquierda a Derecha y de Abajo hacía Arriba: Desde posición x:" + i + ", posición y:" + posicionColumna + 
                        ", Hasta posición x1:"+(i-(letras.length-1))+", posición y1:"+(posicionColumna+(letras.length-1))+"\n";
            }
        }
        return msg;
    }

    private boolean estaDiagonalIzquierdaDerechaInversa(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicialFila = posicionFila;
        int posicionInicialColumna = posicionColumna;
        while (posicionLetras < letras.length && posicionFila >= 0 && posicionColumna < sopas[0].length) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila--;
            posicionColumna++;
        }

        if ((posicionInicialFila + 1) >= letras.length && (sopas[0].length - posicionInicialColumna) >= letras.length) {
            pintarDiagonalIzquierdaDerechaInversa(posicionInicialFila, posicionInicialColumna, letras.length);

        }
        return true;
    }

    private void pintarDiagonalIzquierdaDerechaInversa(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila--;
            columna++;
        }
    }

    public String diagonalesDerechaIzquierda(String palabra) throws Exception {
        if (palabra == null || palabra.isEmpty()) {
            throw new Exception("Error, ingrese una palabra o letra");
        }

        String msg = "";
        char letras[] = palabra.toCharArray();

        for (int i = this.sopas[0].length - 1; i >= 0; i--) {
            msg += vectorDiagonalDerechaIzquierda(i, letras);
            msg += vectorDiagonalDerechaIzquierdaInversa(i, letras);
        }
        return msg;
    }

    private String vectorDiagonalDerechaIzquierda(int posicionColumna, char letras[]) {
        String msg = "";
        for (int i = 0; i < sopas.length; i++) {
            boolean validacion = (sopas.length - i) >= letras.length && (posicionColumna + 1) >= letras.length;
            if (estaDiagonalDerechaIzquierda(i, posicionColumna, letras) && validacion) {
                msg += "- Diagonal Derecha a Izquierda de Arriba hacía Abajo: Desde posición x:" + i + ", posicion y:" + posicionColumna + 
                        ", Hasta posición x:"+(i+(letras.length-1))+", posición y:"+(posicionColumna-(letras.length-1))+"\n";
            }
        }

        return msg;
    }

    private boolean estaDiagonalDerechaIzquierda(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicialFila = posicionFila;
        int posicionInicialColumna = posicionColumna;
        while (posicionLetras < letras.length && posicionFila < sopas.length && posicionColumna >= 0) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila++;
            posicionColumna--;
        }

        if ((sopas.length - posicionInicialFila) >= letras.length && (posicionInicialColumna + 1) >= letras.length) {
            pintarDiagonalDerechaIzquierda(posicionInicialFila, posicionInicialColumna, letras.length);

        }
        return true;
    }

    private void pintarDiagonalDerechaIzquierda(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila++;
            columna--;
        }
    }

    private String vectorDiagonalDerechaIzquierdaInversa(int posicionColumna, char letras[]) {
        String msg = "";

        for (int i = sopas.length - 1; i >= 0; i--) {
            boolean validacion = (i + 1) >= letras.length && (posicionColumna + 1) >= letras.length;
            if (estaDiagonalDerechaIzquierdaInversa(i, posicionColumna, letras) && validacion) {
                msg += "- Diagonal Derecha a Izquierda de Abajo hacía Arriba: Desde posición x:" + i + ", posición y:" + posicionColumna + 
                        ", Hasta posición x:"+(i-(letras.length-1))+", posición y:"+(posicionColumna-(letras.length-1))+"\n";
            }
        }
        return msg;
    }

    private boolean estaDiagonalDerechaIzquierdaInversa(int posicionFila, int posicionColumna, char letras[]) {
        int posicionLetras = 0;
        int posicionInicialFila = posicionFila;
        int posicionInicialColumna = posicionColumna;
        while (posicionLetras < letras.length && posicionFila >= 0 && posicionColumna >= 0) {
            if (sopas[posicionFila][posicionColumna] != letras[posicionLetras]) {
                return false;
            }
            posicionLetras++;
            posicionFila--;
            posicionColumna--;
        }

        if ((posicionInicialFila + 1) >= letras.length && (posicionInicialColumna + 1) >= letras.length) {
            pintarDiagonalDerechaIzquierdaInversa(posicionInicialFila, posicionInicialColumna, letras.length);

        }
        return true;
    }

    private void pintarDiagonalDerechaIzquierdaInversa(int fila, int columna, int tamanioPalabra) {

        for (int i = 0; i < tamanioPalabra; i++) {
            sopaPintada[fila][columna] = 1;
            fila--;
            columna--;
        }
    }

    public void pintar(String palabra) throws Exception {
        horizontales(palabra);
        verticales(palabra);
        diagonalesDerechaIzquierda(palabra);
        diagonalesIzquierdaDerecha(palabra);
    }

    public void generarPDF(String palabra, String nombre) throws FileNotFoundException, IOException, InterruptedException {

        FileOutputStream f = new FileOutputStream(nombre + ".pdf");
        PdfWriter writer = new PdfWriter(f);
        PdfDocument pdfDoc = new PdfDocument(writer);

        Document document = new Document(pdfDoc, PageSize.A4);
        document.setMargins(50, 30, 20, 30);
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont font1 = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

        Table table = new Table(this.sopas[0].length);
        proceso(table, font);
        table.setWidthPercent(70);
        document.add(table);

        try {
            if (encontroPalabra()) {
                Paragraph hor = new Paragraph("Horizontales").setFont(font1);
                Paragraph ver = new Paragraph("Verticales").setFont(font1);
                Paragraph dia = new Paragraph("Diagonales").setFont(font1);

                document.add(new Paragraph("\n" + "Posiciones de la Palabra: " + palabra).setFont(font1));
                document.add(hor);
                document.add(new Paragraph(horizontales(palabra) + "\n").setFont(font));
                document.add(ver);
                document.add(new Paragraph(verticales(palabra) + "\n").setFont(font));
                document.add(dia);
                document.add(new Paragraph(diagonalesIzquierdaDerecha(palabra)).setFont(font));
                document.add(new Paragraph(diagonalesDerechaIzquierda(palabra)).setFont(font));
            } else {
                document.add(new Paragraph("\n" + "La Palabra " + palabra + " no se encontró en la Sopa de Letras").setFont(font1));
            }
            document.close();

        } catch (Exception ex) {
            Logger.getLogger(SopaDeLetras.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void abrirPDF(String nombre) throws IOException {

        int respuesta = JOptionPane.showConfirmDialog(null, "Desea Imprimir PDF",
                "Sopa de letras", JOptionPane.YES_NO_OPTION);
        if (respuesta == 0) {
            File f = new File(nombre + ".pdf");
            Desktop.getDesktop().open(f);
        }else{
            JOptionPane.showMessageDialog(null, "No se abrirá el PDF");
        }
    }

    private void proceso(Table tabla, PdfFont font) {

        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                if (sopaPintada[i][j] == 1) {
                    tabla.addCell(String.valueOf(this.sopas[i][j])).setFont(font);
                    tabla.getCell(i, j).setBackgroundColor(Color.YELLOW);
                } else {
                    tabla.addCell(String.valueOf(this.sopas[i][j])).setFont(font);
                }
            }
        }
    }

    public boolean encontroPalabra() {
        for (int i = 0; i < this.sopas.length; i++) {
            if (palabra(sopaPintada[i])) {
                return true;
            }
        }
        return false;
    }

    private boolean palabra(int vector[]) {

        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 1) {
                return true;
            }
        }
        return false;
    }

    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas
}
